package com.hendisantika.oracleflywaydemo;

import com.hendisantika.oracleflywaydemo.entity.Employee;
import com.hendisantika.oracleflywaydemo.repository.EmployeeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.flywaydb.core.api.configuration.Configuration;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class OracleFlywayDemoApplicationTests {

    Logger log = LogManager.getLogger(OracleFlywayDemoApplicationTests.class);

    @Autowired
    EmployeeRepository repo;

    @Autowired
    DataSource dataSource;

    @Test
    public void contextLoads() {
        Configuration configuration = new ClassicConfiguration();
        configuration.getDataSource();
        final Flyway flyway = new Flyway(configuration);

        log.info("FLYWAY: Starting clean");
        flyway.clean();
        log.info("FLYWAY: Starting migration");
        flyway.migrate();
        log.info("FLYWAY: Migration Complete");
    }

    @Test
    public void test1AddEmployees() {
        Employee emp = new Employee();
        emp.setFullname("HARRY");
        emp.setDeptNo(10);
        emp.setHired(new Date());
        emp.setJob("Sales");
        repo.save(emp);
    }

    @Test
    public void test2ShowEmployees() {
        log.info("Employees:");
        log.info("=========================");
        for (Employee e : repo.findAll()) {
            log.info(e.getFullname());
        }
    }

    @Test
    public void test3CountEmployees() {
        int cnt = (int) repo.count();
        log.info("Count: " + cnt);
        assertEquals(cnt, 4);
    }

    @Test
    public void test4SearchByName() {
        List<Employee> list = repo.findByName("L%");
        int cnt = list.size();
        log.info("Count employees starting with 'L': " + cnt);
        assertEquals(cnt, 2);
    }


}
