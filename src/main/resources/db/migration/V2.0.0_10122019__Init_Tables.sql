 create table ACCOUNT (
        ACCOUNT_ID number(10,0) not null,
        AVAIL_BALANCE float,
        CLOSE_DATE date,
        LAST_ACTIVITY_DATE date,
        OPEN_DATE date not null,
        PENDING_BALANCE float,
        STATUS varchar2(10 char),
        CUST_ID number(10,0),
        OPEN_BRANCH_ID number(10,0) not null,
        OPEN_EMP_ID number(10,0) not null,
        PRODUCT_CD varchar2(10 char) not null,
        primary key (ACCOUNT_ID)
    );

    create table ACC_TRANSACTION (
        TXN_ID number(19,0) not null,
        AMOUNT float not null,
        FUNDS_AVAIL_DATE timestamp not null,
        TXN_DATE timestamp not null,
        TXN_TYPE_CD varchar2(10 char),
        ACCOUNT_ID number(10,0),
        EXECUTION_BRANCH_ID number(10,0),
        TELLER_EMP_ID number(10,0),
        primary key (TXN_ID)
    );

    create table BRANCH (
        BRANCH_ID number(10,0) not null,
        ADDRESS varchar2(30 char),
        CITY varchar2(20 char),
        NAME varchar2(20 char) not null,
        STATE varchar2(10 char),
        ZIP_CODE varchar2(12 char),
        primary key (BRANCH_ID)
    );

    create table BUSINESS (
        INCORP_DATE date,
        NAME varchar2(255 char) not null,
        STATE_ID varchar2(10 char) not null,
        CUST_ID number(10,0) not null,
        primary key (CUST_ID)
    );

    create table CUSTOMER (
        CUST_ID number(10,0) not null,
        ADDRESS varchar2(30 char),
        CITY varchar2(20 char),
        CUST_TYPE_CD varchar2(1 char) not null,
        FED_ID varchar2(12 char) not null,
        POSTAL_CODE varchar2(10 char),
        STATE varchar2(20 char),
        primary key (CUST_ID)
    );

    create table DEPARTMENT (
        DEPT_ID number(10,0) not null,
        NAME varchar2(20 char) not null,
        primary key (DEPT_ID)
    );

    create table EMPLOYEE (
        EMP_ID number(10,0) not null,
        END_DATE date,
        FIRST_NAME varchar2(20 char) not null,
        LAST_NAME varchar2(20 char) not null,
        START_DATE date not null,
        TITLE varchar2(20 char),
        ASSIGNED_BRANCH_ID number(10,0),
        DEPT_ID number(10,0),
        SUPERIOR_EMP_ID number(10,0),
        primary key (EMP_ID)
    );

    create table INDIVIDUAL (
        BIRTH_DATE date,
        FIRST_NAME varchar2(30 char) not null,
        LAST_NAME varchar2(30 char) not null,
        CUST_ID number(10,0) not null,
        primary key (CUST_ID)
    );

    create table OFFICER (
        OFFICER_ID number(10,0) not null,
        END_DATE date,
        FIRST_NAME varchar2(30 char) not null,
        LAST_NAME varchar2(30 char) not null,
        START_DATE date not null,
        TITLE varchar2(20 char),
        CUST_ID number(10,0),
        primary key (OFFICER_ID)
    );

    create table PRODUCT (
        PRODUCT_CD varchar2(10 char) not null,
        DATE_OFFERED date,
        DATE_RETIRED date,
        NAME varchar2(50 char) not null,
        PRODUCT_TYPE_CD varchar2(255 char),
        primary key (PRODUCT_CD)
    );

    create table PRODUCT_TYPE (
        PRODUCT_TYPE_CD varchar2(255 char) not null,
        NAME varchar2(50 char),
        primary key (PRODUCT_TYPE_CD)
    );

    alter table ACCOUNT 
        add constraint ACCOUNT_CUSTOMER_FK 
        foreign key (CUST_ID) 
        references CUSTOMER;

    alter table ACCOUNT 
        add constraint ACCOUNT_BRANCH_FK 
        foreign key (OPEN_BRANCH_ID) 
        references BRANCH;

    alter table ACCOUNT 
        add constraint ACCOUNT_EMPLOYEE_FK 
        foreign key (OPEN_EMP_ID) 
        references EMPLOYEE;

    alter table ACCOUNT 
        add constraint ACCOUNT_PRODUCT_FK 
        foreign key (PRODUCT_CD) 
        references PRODUCT;

    alter table ACC_TRANSACTION 
        add constraint ACC_TRANSACTION_ACCOUNT_FK 
        foreign key (ACCOUNT_ID) 
        references ACCOUNT;

    alter table ACC_TRANSACTION 
        add constraint ACC_TRANSACTION_BRANCH_FK 
        foreign key (EXECUTION_BRANCH_ID) 
        references BRANCH;

    alter table ACC_TRANSACTION 
        add constraint ACC_TRANSACTION_EMPLOYEE_FK 
        foreign key (TELLER_EMP_ID) 
        references EMPLOYEE;

    alter table BUSINESS 
        add constraint BUSINESS_EMPLOYEE_FK 
        foreign key (CUST_ID) 
        references CUSTOMER;

    alter table EMPLOYEE 
        add constraint EMPLOYEE_BRANCH_FK 
        foreign key (ASSIGNED_BRANCH_ID) 
        references BRANCH;

    alter table EMPLOYEE 
        add constraint EMPLOYEE_DEPARTMENT_FK 
        foreign key (DEPT_ID) 
        references DEPARTMENT;

    alter table EMPLOYEE 
        add constraint EMPLOYEE_EMPLOYEE_FK 
        foreign key (SUPERIOR_EMP_ID) 
        references EMPLOYEE;

    alter table INDIVIDUAL 
        add constraint INDIVIDUAL_CUSTOMER_FK 
        foreign key (CUST_ID) 
        references CUSTOMER;

    alter table OFFICER 
        add constraint OFFICER_CUSTOMER_FK 
        foreign key (CUST_ID) 
        references CUSTOMER;

    alter table PRODUCT 
        add constraint PRODUCT_PRODUCT_TYPE_FK 
        foreign key (PRODUCT_TYPE_CD) 
        references PRODUCT_TYPE;

    create sequence hibernate_sequence;


 
-- ======================================================================== 
-- ========================================================================
-- ========================================================================
