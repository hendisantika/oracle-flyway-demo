create sequence emp_seq start with 1000 increment by 1 cache 100;

create table employee (
  emp_no    number(4,0) DEFAULT emp_seq.nextval,
  fullname    varchar2(10),
  job      varchar2(9),
  manager      number(4,0),
  hired date,
  salary      number(10,2),
  comm     number(7,2),
  dept_no   number(2,0),
  constraint pk_emp primary key (emp_no)
);

insert into employee (fullname, job, manager, hired, salary, comm, dept_no) values ('LARRY', 'CEO', null, sysdate-4000, 10000, null, 10);
insert into employee (fullname, job, manager, hired, salary, comm, dept_no) values ('KING', 'Sales', 1000, sysdate-3000, 5000, null, 30);
insert into employee (fullname, job, manager, hired, salary, comm, dept_no) values ('LINDA', 'Dev', 1000, sysdate-2000, 6000, null, 30);
commit;

--grant select, insert, update, delete on emp to demo_rw;
--grant select on emp to demo_ro;