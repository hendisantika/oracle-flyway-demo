package com.hendisantika.oracleflywaydemo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : oracle-flyway-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/12/19
 * Time: 17.09
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "EMP_SEQ")
    private int empNo;
    private String fullname;
    private String job;

    @Column(name = "manager", nullable = true)
    private Integer managerNo;
    private Date hired;

    @Column(name = "salary")
    private Integer salary;

    @Column(nullable = true)
    private Integer comm;

    private Integer deptNo;
}
