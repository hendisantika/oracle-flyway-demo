package com.hendisantika.oracleflywaydemo.controller;

import com.hendisantika.oracleflywaydemo.entity.Employee;
import com.hendisantika.oracleflywaydemo.repository.EmployeeRepository;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : oracle-flyway-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/12/19
 * Time: 17.49
 */

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class EmployeeController {
    private final EmployeeRepository employeeRepository;

    @GetMapping
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @PostMapping
    public List<Employee> saveData(@Valid @RequestBody Employee employee) {
        employeeRepository.save(employee);
        return employeeRepository.findAll();
    }
}
