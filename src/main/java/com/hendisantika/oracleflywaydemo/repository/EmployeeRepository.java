package com.hendisantika.oracleflywaydemo.repository;

import com.hendisantika.oracleflywaydemo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : oracle-flyway-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/12/19
 * Time: 17.47
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query("SELECT e FROM Employee e  WHERE lower(e.fullname) like lower(:fullname)")
    List<Employee> findByName(@Param("fullname") String fullname);
}