# Oracle Flyway Demo

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/oracle-flyway-demo.git`
2. Navigate to folder: `cd oracle-flyway-demo`
3. Run the application: `mvn clean spring-boot:run`

Article Link
* https://www.oracle.com/br/technical-resources/articles/database-performance/oracle-db19c-com-docker.html
* https://www.doag.org/formes/pubfiles/9611986/2017-INF-Klaus_Thielen-Oracle-Datenbank_im_Docker_Container-Praesentation.pdf
* https://infohub.delltechnologies.com/l/oracle-in-docker-containers-managed-by-kubernetes-1/step-3-run-the-oracle-12c-container-on-docker-7
* https://www.dontesta.it/en/2020/03/15/how-to-setup-docker-container-oracle-database-19c-for-liferay-development-environment/
* https://www.oracle.com/br/technical-resources/articles/database-performance/oracle-db19c-com-docker.html
